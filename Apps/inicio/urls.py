from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

app_name = 'inicio'
urlpatterns = [
    path('person/', login_required(views.Person), name='person'),
    path('FiltrarCategorias/<int:id>', login_required(views.FiltrarCategorias), name="FiltrarCategorias"),
    path('FiltrarTipoEvento/<int:id>', login_required(views.FiltrarTipoEvento), name="FiltrarTipoEvento"),

]