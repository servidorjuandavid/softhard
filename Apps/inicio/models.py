from django.db import models
from django.contrib.auth.models import User


class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    telefono = models.CharField(max_length=15)
    foto = models.ImageField(upload_to='perfil/')
    fotoPortada = models.ImageField(upload_to='perfil/')
    profesion = models.CharField(max_length=50)


