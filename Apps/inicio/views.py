from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import login, logout
from django.views.generic.edit import FormView
from .forms import FormularioLogin
from Apps.eventos.models import Producto, TipoProducto, Estado

def Inicio(request):
    template_name = 'index.html'
    return render(request, ('index.html'))

def FiltrarTipoEvento(request, id):
    q = Producto.objects.filter(TipoProducto_id=id).order_by('-FechaCreacion')
    t = TipoProducto.objects.all()
    c = Estado.objects.all()
    contexto = {'datos': q, 'tipos': t, 'categorias': c}
    return render(request, 'inicio/person.html', contexto)

def FiltrarCategorias(request, id):
    q = Producto.objects.filter(Estado_id=id).order_by('-FechaCreacion')
    t = TipoProducto.objects.all()
    c = Estado.objects.all()
    contexto = {'datos': q, 'tipos': t, 'categorias': c}
    return render(request, 'inicio/person.html', contexto)

# Create your views here.


def Person(request):
    t = TipoProducto.objects.all()
    c = Estado.objects.all()
    q = Producto.objects.all().order_by('-FechaCreacion')
    contexto = {'datos': q, 'tipos': t, 'categorias': c}
    return render(request, ('inicio/person.html'), contexto)


class Login(FormView):
    template_name = 'inicio/login.html'
    form_class = FormularioLogin

    success_url = reverse_lazy('inicio:person')

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(Login, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(Login, self).form_valid(form)

def logoutUsuario(request):
    logout(request)
    return HttpResponseRedirect('/')


