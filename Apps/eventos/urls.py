from django.urls import path
from django.contrib.auth.decorators import login_required
from . import views

app_name = 'eventos'
urlpatterns = [
    path('CrearEvento/', login_required(views.CrearEvento.as_view()), name='CrearEvento'),
    path('listaEventos/<int:id>', login_required(views.ListarEventos), name='listaEventos'),
    path('listarMantenimientos/<int:id>', login_required(views.ListarMantenimientos), name='listarMantenimientos'),
    path('listarBackups/<int:id>', login_required(views.ListarBackups), name='listarBackups'),
    path('EditarEvento/<int:pk>/', login_required(views.EditarEvento.as_view()), name='EditarEvento'),
    path('EliminarEvento/<int:pk>/', login_required(views.EliminarEvento.as_view()), name='EliminarEvento'),
    path('DetalleEvento/<int:pk>/', login_required(views.DetalleEvento.as_view()), name='DetalleEvento'),
 
    path('CrearMantenimiento/', login_required(views.CrearMantenimiento.as_view()), name='CrearMantenimiento'),
    path('EditarMantenimiento/<int:pk>/', login_required(views.EditarMantenimiento.as_view()), name='EditarMantenimiento'),
    path('EliminarMantenimiento/<int:pk>/', login_required(views.EliminarMantenimiento.as_view()), name='EliminarMantenimiento'),
    path('DetalleMantenimiento/<int:pk>/', login_required(views.DetalleMantenimiento.as_view()), name='DetalleMantenimiento'),

    path('CrearBackup/', login_required(views.CrearBackup.as_view()), name='CrearBackup'),
    path('EditarBackup/<int:pk>/', login_required(views.EditarBackup.as_view()), name='EditarBackup'),
    path('EliminarBackup/<int:pk>/', login_required(views.EliminarBackup.as_view()), name='EliminarBackup'),
    path('DetalleBackup/<int:pk>/', login_required(views.DetalleBackup.as_view()), name='DetalleBackup'),
    
]
