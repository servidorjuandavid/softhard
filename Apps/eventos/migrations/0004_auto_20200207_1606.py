# Generated by Django 3.0 on 2020-02-07 21:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0003_auto_20200207_1020'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='Estado',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='eventos.Estado'),
        ),
    ]
