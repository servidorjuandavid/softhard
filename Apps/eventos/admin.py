from django.contrib import admin
from Apps.eventos.models import Producto,TipoProducto,ModeloProducto, Ubicacion, Proveedor, Propietario, Area, Contrato, Estado, Mantenimiento, Backup
# Register your models here.

admin.site.register(Producto)
admin.site.register(TipoProducto)
admin.site.register(ModeloProducto)
admin.site.register(Ubicacion)
admin.site.register(Proveedor)
admin.site.register(Propietario)
admin.site.register(Area)
admin.site.register(Contrato)
admin.site.register(Estado)
admin.site.register(Mantenimiento)
admin.site.register(Backup)

