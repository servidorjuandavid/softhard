from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views import generic
from django.contrib.auth.models import User
from .models import Producto, TipoProducto, Estado, Mantenimiento, Backup
from .forms import FormProducto, FormMantenimiento, FormBackup
 

class CrearEvento(generic.CreateView):
    model = Producto
    form_class = FormProducto
    template_name = 'eventos/form.html'
    success_url = reverse_lazy('inicio:person')


class EditarEvento(generic.UpdateView):
    model = Producto
    form_class = FormProducto
    template_name = 'eventos/edit.html'

    def get_success_url(self):
        return reverse('eventos:listaEventos', kwargs={"id": self.request.user.id})


class EliminarEvento(generic.DeleteView):
    model = Producto
    template_name = 'eventos/evento_confirm_delete.html'
    
    def get_success_url(self):
        return reverse('eventos:listaEventos', kwargs={"id": self.request.user.id})


class DetalleEvento(generic.DetailView):
    model = Producto
    template_name = 'eventos/mostrar.html'



'''  Vistas Mantenimientos'''

class CrearMantenimiento(generic.CreateView):
    model = Mantenimiento
    form_class = FormMantenimiento
    template_name = 'Mantenimientos/form.html'
    success_url = reverse_lazy('inicio:person')


class EditarMantenimiento(generic.UpdateView):
    model = Mantenimiento
    form_class = FormMantenimiento
    template_name = 'Mantenimientos/edit.html'

    def get_success_url(self):
        return reverse('eventos:listarMantenimientos', kwargs={"id": self.request.user.id})


class EliminarMantenimiento(generic.DeleteView):
    model = Mantenimiento
    template_name = 'Mantenimientos/evento_confirm_delete.html'
    
    def get_success_url(self):
        return reverse('eventos:listarMantenimientos', kwargs={"id": self.request.user.id})


class DetalleMantenimiento(generic.DetailView):
    model = Mantenimiento
    template_name = 'Mantenimientos/mostrar.html'

'''  Vistas Backups'''

class CrearBackup(generic.CreateView):
    model = Backup
    form_class = FormBackup
    template_name = 'Backups/form.html'
    success_url = reverse_lazy('inicio:person')


class EditarBackup(generic.UpdateView):
    model = Backup
    form_class = FormBackup
    template_name = 'Backups/edit.html'

    def get_success_url(self):
        return reverse('eventos:listarBackups', kwargs={"id": self.request.user.id})


class EliminarBackup(generic.DeleteView):
    model = Backup
    template_name = 'Backups/evento_confirm_delete.html'
    
    def get_success_url(self):
        return reverse('eventos:listarMantenimientos', kwargs={"id": self.request.user.id})


class DetalleBackup(generic.DetailView):
    model = Backup
    template_name = 'Backups/mostrar.html'



def ListarEventos(request, id):
    queryset = Producto.objects.all().order_by('-FechaCreacion')
    context = {'datos': queryset}
    return render(request, ('Administrador/adminEventos.html'), context)


def ListarMantenimientos(request, id):
    queryset = Mantenimiento.objects.all().order_by('-FechaCreacion')
    context = {'datos': queryset}
    return render(request, ('Administrador/listarMantenimientos.html'), context)

def ListarBackups(request, id):
    queryset = Backup.objects.all().order_by('-FechaCreacion')
    context = {'datos': queryset}
    return render(request, ('Administrador/listarBackups.html'), context)

def selectCategoria(request):
    contexto = {'categorias': ""}
    return render(request, ('inicio/person.html'), contexto)


def selectTipoEvento(request):
    a = TipoProducto.objects.all()
    q = Estado.objects.all()
    contexto = {'tipos': a, 'categorias': q}
    return render(request, ('inicio/person.html'), contexto)

