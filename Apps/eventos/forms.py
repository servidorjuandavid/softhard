from django import forms
from .models import Producto, Mantenimiento, Backup
from django.core.exceptions import ValidationError
import datetime


class FormProducto(forms.ModelForm):
    class Meta:
        model = Producto
        fields = [
            'Serial',
            'Imei',
            'Asignado',
            'Descripcion',
            'Cantidad',
            'TipoProducto',
            'ModeloProducto',
            'Ubicacion',
            'Propietario',
            'Proveedor',
            'Contrato',
            'Area',
            'Estado',
            'Foto',
        ]
        widgets = {
            'Serial': forms.TextInput(attrs={'class': 'form-control'}),
            'Imei': forms.TextInput(attrs={'class': 'form-control'}),
            'Asignado': forms.TextInput(attrs={'class': 'form-control'}),
            'Descripcion': forms.DateInput(attrs={'class': 'form-control'}),
            'Cantidad': forms.TextInput(attrs={'class': 'form-control', 'type': 'number' ,'min':'1'}),
            'TipoProducto': forms.Select(attrs={'class': 'form-control'}),
            'ModeloProducto': forms.Select(attrs={'class': 'form-control'}),
            'Ubicacion': forms.Select(attrs={'class': 'form-control'}),
            'Propietario': forms.Select(attrs={'class': 'form-control'}),
            'Proveedor': forms.Select(attrs={'class': 'form-control'}),
            'Contrato': forms.Select(attrs={'class': 'form-control'}),
            'Area': forms.Select(attrs={'class': 'form-control'}),
            'Estado': forms.Select(attrs={'class': 'form-control'}),


        }


class FormMantenimiento(forms.ModelForm):
    class Meta:
        model = Mantenimiento
        fields = [
            'ReporteNoveda',
            'ReporteSolucion',
            'Observaciones',
            'DiaEnvio',
            'DiaRegreso',
            'Producto',
        ]
        widgets = {
            'ReporteNoveda': forms.TextInput(attrs={'class': 'form-control'}),
            'ReporteSolucion': forms.TextInput(attrs={'class': 'form-control'}),
            'Observaciones': forms.TextInput(attrs={'class': 'form-control'}),
            'DiaEnvio': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'DiaRegreso': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'Producto': forms.Select(attrs={'class': 'form-control'}),

        }



class FormBackup(forms.ModelForm):
    class Meta:
        model = Backup
        fields = [
            'ReporteNoveda',
            'Observaciones',
            'DiaEnvio',
            'DiaRegreso',
            'Producto',
        ]
        widgets = {
            'ReporteNoveda': forms.TextInput(attrs={'class': 'form-control'}),
            'Observaciones': forms.TextInput(attrs={'class': 'form-control'}),
            'DiaEnvio': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'DiaRegreso': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'Producto': forms.Select(attrs={'class': 'form-control'}),

        }