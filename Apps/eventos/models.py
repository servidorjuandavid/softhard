from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.


class TipoProducto(models.Model):
    NombreTipo = models.CharField(max_length=100, blank=False, null=False)
    Fabricante = models.CharField(max_length=150, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.NombreTipo)

class ModeloProducto(models.Model):
    NombreModelo = models.CharField(max_length=100, blank=False, null=False)


    def __str__(self):
        return '{}'.format(self.NombreModelo)

class Ubicacion(models.Model):
    NombreUbicacion = models.CharField(max_length=100, blank=False, null=False)
    DireccionUbicacion = models.CharField(max_length=150, blank=True, null=True)
    TelefonoUbicacion = models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.NombreUbicacion)

class Proveedor(models.Model):
    NombreProveedor = models.CharField(max_length=100, blank=False, null=False)
    DireccionProveedor = models.CharField(max_length=150, blank=True, null=True)
    TelefonoProveedor = models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.NombreProveedor)

class Propietario(models.Model):
    NombrePropietario = models.CharField(max_length=100, blank=False, null=False)
    DireccionPropietario = models.CharField(max_length=150, blank=True, null=True)
    TelefonoPropietario= models.CharField(max_length=25, blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.NombrePropietario)

class Area(models.Model):
    NombreArea = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return '{}'.format(self.NombreArea)

class Contrato(models.Model):
    NombreContrato = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return '{}'.format(self.NombreContrato)
class Estado(models.Model):
    NombreEstado = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return '{}'.format(self.NombreEstado)

class Producto(models.Model):
    Serial = models.CharField(max_length=200, blank=False, null=False)
    Imei = models.CharField(max_length=200, blank=False, null=False)
    Asignado = models.CharField(max_length=200, blank=False, null=True)
    Descripcion = models.CharField(max_length=300, blank=True, null=True)
    Foto = models.ImageField(upload_to='Fotos/')
    FechaCreacion = models.DateTimeField(auto_now=False, auto_now_add=True)
    Cantidad = models.IntegerField(default=1)
    TipoProducto = models.ForeignKey(TipoProducto, null=True, blank=True, on_delete=models.CASCADE)
    ModeloProducto = models.ForeignKey(ModeloProducto, null=True, blank=True, on_delete=models.CASCADE)
    Ubicacion = models.ForeignKey(Ubicacion, null=True, blank=True, on_delete=models.CASCADE)
    Propietario = models.ForeignKey(Propietario, null=True, blank=True, on_delete=models.CASCADE)
    Proveedor = models.ForeignKey(Proveedor, null=True, blank=True, on_delete=models.CASCADE)
    Contrato = models.ForeignKey(Contrato, null=True, blank=True, on_delete=models.CASCADE)
    Area = models.ForeignKey(Area, null=True, blank=True, on_delete=models.CASCADE)
    Estado = models.ForeignKey(Estado, null=False, blank=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)


    def __str__(self):
        return '{}'.format(self.Serial)

class Mantenimiento(models.Model):
    ReporteNoveda = models.CharField(max_length=400, blank=True, null=False)
    ReporteSolucion = models.CharField(max_length=400, blank=True, null=True)
    Observaciones = models.CharField(max_length=300, blank=True, null=True)
    FechaCreacion = models.DateTimeField(auto_now=False, auto_now_add=True)
    DiaEnvio = models.DateField(blank=False, null=False)
    DiaRegreso = models.DateField(blank=True, null=True)
    Producto = models.ForeignKey(Producto, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.FechaCreacion)

class Backup(models.Model):
    ReporteNoveda = models.CharField(max_length=400, blank=True, null=False)
    Observaciones = models.CharField(max_length=300, blank=True, null=True)
    FechaCreacion = models.DateTimeField(auto_now=False, auto_now_add=True)
    DiaEnvio = models.DateField(blank=False, null=False)
    DiaRegreso = models.DateField(blank=True, null=True)
    Producto = models.ForeignKey(Producto, null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.FechaCreacion)


